# IObject.js
**Version: 0.3.0**  
**Notes:**  

* Can be used with AMD libraries (like Require.JS) or CommonJS (Node.js)

# Description
**IObject.js** is the lightweight library providing a bunch of methods to simplify JavaScript OOP. This is expired by IO programming language and uses **Prototype-based OOP** concepts.

## Cloning objects
To start creating objects hierarchy you need to use "_clone_" method of _IObject_:
```js
var Shape = IObject.clone();
```
Then you can add some methods to created object:
```js
Shape.draw = function () {
    var x = this.orgin.x, y = this.origin.y;
    // Some code here...
};
```
This _Shape_ object is going to be a prototype for concrete shapes objects. To create concrete shape object we need to clone the prototype:
```js
var concreteShape = Shape.clone();
```
Then you can define specific properties for this _concreteShape_ object:
```js
concreteShape.origin = {
    x: 120,
    y: 120
};
```
Now we're ready to use our object: 
```js
concreteShape.draw();
```  

## Initializing objects (some syntactic sugar)
To automate the initialization process for newly created objects you can define "_init_" method in the prototype-object:
```js
Shape.init = function (origin) {
    this.origin = origin;
};
```
As the bottom line there are two ways properties can be set to the object:  
    
1. Setting properties on the run, right after cloning prototype;  
2. Performing all initialization actions and settings properties in "_init_" method.

To call "_init_" method we can use prototype's "_new_" method instead of "_clone_". In this case we going to have such code to create our concrete object:
```js
var concreteShape = Shape.new({
    x: 120,
    y: 120
});
```
## Inheritance
To inherit from existing object... well, we just need to _clone_ it. Like this:
```js
var Rect = Shape.clone();
```
This line of code creates new Rect object with Shape object as prototype.  
To give to our "child" properties of our "parent" we need to call prototype's "_init_" function while initializing our newly created clone. This can be done in its own "_init_" method:
```js
Rect.init = function (origin, size) {
    Shape.init.call(this, origin);
    // Some initialization code...
};
```
And that's all for inheritance... Pure and simple.

## Object's identity (or Who's your daddy?)
In case we need to know whether one object has some "parental" relationships with the other we can use standard [Object.isPrototypeOf](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/isPrototypeOf) method:
```js
var rect = Rect.new(origin, size);
Rect.isPrototypeOf(rect);     // true
Shape.isPrototypeOf(rect);    // true
IObject.isPrototypeOf(rect);  // true
```

## Encapsulation
Actually there is no encapsulation in Prototype-based OOP concept.  
Anyway for defining private/protected properties some naming conventions can be used (like "_" (underscode) or "__" (double underscore) prefixes for property names).

## Additional functionality
### Mixins
All the objects created with IObject support "_mixin_" method. Using this method you can make a [deep copy](http://en.wikipedia.org/wiki/Object_copy#Deep_copy) of one object's slots to another object.
```js
Shape.mixin(Movable);
```
This line will copy all slots of _Movable_ object to the _Shape_.  
Also we can use chainig and add few mixins:
```js
var Rect = Shape.clone()
                 .mixin(Movable)
                 .mixin(Rotatable);
```
