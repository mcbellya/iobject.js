(function (root, factory) {
    'use strict';
    
    /*global define, exports:true */
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof exports === 'object') {
        exports = factory();
    } else {
        root.IObject = factory();
    }
    
}(this, function () {
    'use strict';

    var
        isFunc = function (op) { return typeof op === 'function'; },
        isObj  = function (op) { return typeof op === 'object'; },
        isArr  = function (op) { return op instanceof Array; },
        create = isFunc(Object.create) ? Object.create : (function () {
            var Mediator = function () {};
            return function (parent) {
                if (!arguments.length) { throw new Error('Second argument not supported'); }
                if (!parent) { throw new Error('Cannot set a null [[Prototype]]'); }
                if (!isObj(parent)) { throw new TypeError('Argument must be an object'); }
                Mediator.prototype = parent;
                return new Mediator();
            };
        }()),
        initialize = function (obj, params) {
            if (isFunc(obj.init)) {
                obj.init(params);
            }
            return obj;
        },
        deepCopy = function (src) {
            var result;
            if (isArr(src)) {
                result = [];
                for (var i = 0; i < src.length; i += 1) {
                    result[i] = deepCopy(src[i]);
                }
                return result;
            } else if (isObj(src)) {
                result = {};
                for (var p in src) {
                    if (src.hasOwnProperty(p)) {
                        result[p] = deepCopy(src[p]);
                    }
                }
                return result;
            }
            return src;
        },
        extend = function (dest, src) {
            for (var p in src) {
                if (src.hasOwnProperty(p) && p !== 'init') {
                    dest[p] = deepCopy(src[p]);
                }
            }
            return dest;
        };

    return {
        'version': "0.3.0",
        'init': function () {},
        'clone': function () { return create(this); },
        'new': function (params) { return initialize(this.clone(), params); },
        'mixin': function (obj) { return extend(this, obj); }
    };

}));